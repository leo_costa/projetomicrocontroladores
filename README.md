# Pong in 8051 Assembly

This is a project for a microcontrollers class I took during graduation.

You can see a demonstration in the [Youtube
Video](https://www.youtube.com/watch?v=U9yfLN2kTI4).

## Documentation

The code was first designed using flowcharts, to avoid any errors in logic.
Each flowchart can be seen below.

### Basic Functions

This flowchart implements some basic functions, such as: 
- Displaying the game matrix (MostraMatriz),
- Handling Timer 0 (TrataT0), 
- The main loop (Principal).

![Basic Functions](./Fluxogramas/FuncoesBasicas.png) 

### Game Functions

This flowchart implements the game functions, such as:
- Moving the ball (MoveBola),
- Reset the game (ResetaJogo),
- Increment the scores for each player (IncPontoD, IncPontoE),
- Debouncing the switches for each player (DebounceP[20,21,22,23]),
- Moving the racket up and down for both players (SobeDireita, DesceDireita, SobeEsquerda, DesceEsquerda),
- Waits for the player to release the switch (AnalisaP[20, 21, 22, 23]), 
- Reading each input and moving the racket accordingly (RaqueteDireitaCima, RaqueteDireitaBaixo, RaqueteEsquerdaCima, RaqueteEsquerdaBaixo).

![Game Functions](./Fluxogramas/FuncoesJogo.png) 

### LCD Functions

This flowchart implements the LCD functions, such as:

- Initializing the LCD display (IniciaLCD),
- Send a pulse in the enable pin (PulsarEnable),
- Send a command (EnviaComando),
- Write a character (EscreveCaractere),
- Write the score (EscrevePlacar),
- Show that the left player won (EsquerdaGanhou),
- Show that the right player won (DireitaGanhou),
- Show the 'Win' word (Ganhou),
- Show the left score (EscrevePontosEsquerda),
- Show the right score (EscrevePontosDireita),
- Show the trophy (EscreveTrofeu),
- Create the trophy symbol (CriaSimboloTrofeu).

![LCD Functions](./Fluxogramas/FuncoesLCD.png) 

The trophy symbol model can be seen on [this excel sheet](./docs/Matriz+SimboloTrofeu.xlsx) 

### Delay Functions

This flowchart implements software delay functions that are used in the game.

![Delay Functions](./Fluxogramas/Delays.png) 

## Simulation

The game was simulated in PROTEUS software, it is composed of:

- A 8051 microcontroller with a crystal oscillator,
- Four push buttons, which control the racket for each player (up and down),
- A 16x2 LCD,
- A 8x8 LED matrix.

The schematic can be seen below.

![Simulation](./PROTEUS/circuito.png) 
