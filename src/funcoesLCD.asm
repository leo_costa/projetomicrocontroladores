; Arquivo com todas as funções responsáveis por lidar com o
; display de LCD
;=============================================================================
; Pinos utilizados
; P0 |7  6   5   4  3   2   1   0
; LCD|E  RW  RS  -  D7  D6  D5  D4
;=============================================================================
$INCLUDE(delays.asm)

; Gera um pulso no pino de Enable do LCD
ORG 0170h
PulsarEnable:
	SETB P0.7
	LCALL DelayEnable
	CLR P0.7
RET

; Inicializa o LCD
ORG 0180h
IniciaLCD:
	LCALL Delay15

	MOV P0, #03h
	LCALL PulsarEnable

	LCALL Delay4

	MOV P0, #03h
	LCALL PulsarEnable

	LCALL Delay01

	MOV P0, #03h
	LCALL PulsarEnable
	
	LCALL Delay01

	MOV P0, #02h
	LCALL PulsarEnable

	LCALL Delay01

	MOV P0, #02h
	LCALL PulsarEnable

	LCALL Delay01

	MOV P0, #0Ch ; 0 0 0 0 N F X X
	LCALL PulsarEnable

	LCALL Delay01

	MOV P0, #00h
	LCALL PulsarEnable

	LCALL Delay01

	MOV P0, #08h
	LCALL PulsarEnable

	LCALL Delay01

	MOV P0, #00h
	LCALL PulsarEnable

	LCALL Delay01

	MOV P0, #01h
	LCALL PulsarEnable

	LCALL Delay01

	MOV P0, #00h
	LCALL PulsarEnable
	
	LCALL Delay01

	MOV P0, #06h ; 0 0 0 0 0 1 I/D S
	LCALL PulsarEnable

	LCALL Delay01

	MOV P0, #00h
	LCALL PulsarEnable

	LCALL Delay1

	MOV P0, #0Ch ;0 0 0 0 1 D C B
	LCALL PulsarEnable

	LCALL Delay1
RET

; Envia o comando contido no reg. 50h
ORG 0220h
EnviaComando:
;High Nibble
	MOV P0, #00h
	MOV A, 50h
	ANL A, #0F0h
	RR A
	RR A
	RR A
	RR A
	ORL P0, A
	LCALL PulsarEnable
	LCALL Delay1
;Low Nibble
	MOV P0, #00h
	MOV A, 50h
	ANL A, #0Fh
	ORL P0, A
	LCALL PulsarEnable
	LCALL Delay1
RET

; Escreve o caractere armazenado no reg. 50h
ORG 0270h
EscreveCaractere:
;High Nibble
	MOV P0, #20h
	MOV A, 50h
	ANL A, #0F0h
	RR A
	RR A
	RR A
	RR A
	ORL P0, A
	LCALL PulsarEnable
	LCALL Delay1
;Low Nibble
	MOV P0, #20h
	MOV A, 50h
	ANL A, #0Fh
	ORL P0, A
	LCALL PulsarEnable
	LCALL Delay1
RET

; Limpa a tela do LCD
ORG 02C0h
LimpaTela:
	MOV 50h, #01h
	LCALL EnviaComando
RET

; Cria o símbolo de troféu na CGRAM do LCD
ORG 02E0h
CriaSimboloTrofeu:
	MOV 50h, #40h
	LCALL EnviaComando

; A
	MOV 50h, #14h
	LCALL EscreveCaractere
	MOV 50h, #08h
	LCALL EscreveCaractere
	MOV 50h, #14h
	LCALL EscreveCaractere
	MOV 50h, #00h
	LCALL EscreveCaractere
	MOV 50h, #03h
	LCALL EscreveCaractere
	MOV 50h, #02h
	LCALL EscreveCaractere
	MOV 50h, #02h
	LCALL EscreveCaractere
	MOV 50h, #01h
	LCALL EscreveCaractere

	MOV 50h, #48h
	LCALL EnviaComando
; B
	MOV 50h, #03h
	LCALL EscreveCaractere
	MOV 50h, #04h
	LCALL EscreveCaractere
	MOV 50h, #0Fh
	LCALL EscreveCaractere
	MOV 50h, #18h
	LCALL EscreveCaractere
	MOV 50h, #08h
	LCALL EscreveCaractere
	MOV 50h, #08h
	LCALL EscreveCaractere
	MOV 50h, #08h
	LCALL EscreveCaractere
	MOV 50h, #08h
	LCALL EscreveCaractere

	MOV 50h, #50h
	LCALL EnviaComando
; C
	MOV 50h, #18h
	LCALL EscreveCaractere
	MOV 50h, #04h
	LCALL EscreveCaractere
	MOV 50h, #1Eh
	LCALL EscreveCaractere
	MOV 50h, #03h
	LCALL EscreveCaractere
	MOV 50h, #02h
	LCALL EscreveCaractere
	MOV 50h, #02h
	LCALL EscreveCaractere
	MOV 50h, #02h
	LCALL EscreveCaractere
	MOV 50h, #02h
	LCALL EscreveCaractere

	MOV 50h, #58h
	LCALL EnviaComando
; D
	MOV 50h, #05h
	LCALL EscreveCaractere
	MOV 50h, #02h
	LCALL EscreveCaractere
	MOV 50h, #05h
	LCALL EscreveCaractere
	MOV 50h, #00h
	LCALL EscreveCaractere
	MOV 50h, #18h
	LCALL EscreveCaractere
	MOV 50h, #08h
	LCALL EscreveCaractere
	MOV 50h, #08h
	LCALL EscreveCaractere
	MOV 50h, #10h
	LCALL EscreveCaractere

	MOV 50h, #60h
	LCALL EnviaComando
; E
	MOV 50h, #00h
	LCALL EscreveCaractere
	MOV 50h, #00h
	LCALL EscreveCaractere
	MOV 50h, #04h
	LCALL EscreveCaractere
	MOV 50h, #0Ch
	LCALL EscreveCaractere
	MOV 50h, #04h
	LCALL EscreveCaractere
	MOV 50h, #04h
	LCALL EscreveCaractere
	MOV 50h, #04h
	LCALL EscreveCaractere
	MOV 50h, #04h
	LCALL EscreveCaractere

	MOV 50h, #68h
	LCALL EnviaComando
; F
	MOV 50h, #18h
	LCALL EscreveCaractere
	MOV 50h, #0Ch
	LCALL EscreveCaractere
	MOV 50h, #06h
	LCALL EscreveCaractere
	MOV 50h, #01h
	LCALL EscreveCaractere
	MOV 50h, #01h
	LCALL EscreveCaractere
	MOV 50h, #01h
	LCALL EscreveCaractere
	MOV 50h, #03h
	LCALL EscreveCaractere
	MOV 50h, #07h
	LCALL EscreveCaractere

	MOV 50h, #70h
	LCALL EnviaComando
; G
	MOV 50h, #03h
	LCALL EscreveCaractere
	MOV 50h, #06h
	LCALL EscreveCaractere
	MOV 50h, #0Ch
	LCALL EscreveCaractere
	MOV 50h, #10h
	LCALL EscreveCaractere
	MOV 50h, #10h
	LCALL EscreveCaractere
	MOV 50h, #10h
	LCALL EscreveCaractere
	MOV 50h, #18h
	LCALL EscreveCaractere
	MOV 50h, #1Ch
	LCALL EscreveCaractere

	MOV 50h, #80h
	LCALL EnviaComando
RET

; Escreve PLACAR no LCD
ORG 0480h
EscrevePlacar:
; Move o cursor
	MOV 50h, #85h
	LCALL EnviaComando
; Escreve PLACAR
	MOV 50h, #50h; P
	LCALL EscreveCaractere
	MOV 50h, #4Ch; L
	LCALL EscreveCaractere
	MOV 50h, #41h; A
	LCALL EscreveCaractere
	MOV 50h, #43h; C
	LCALL EscreveCaractere
	MOV 50h, #41h; A
	LCALL EscreveCaractere
	MOV 50h, #52h; R
	LCALL EscreveCaractere
RET

; Escreve no LCD a pontuação do jogador da esquerda
; A pontuação fica armazenada no reg. 51h
ORG 04C0h
EscrevePontosEsquerda:
; Move o cursor
	MOV 50h, #0C5h
	LCALL EnviaComando
	MOV 50h, 51h
	ORL 50h, #30h
	LCALL EscreveCaractere
RET

; Escreve no LCD a pontuação do jogador da direita
; A pontuação fica armazenada no reg. 52h
ORG 04D0h
EscrevePontosDireita:
; Coloca o cursor na posicao 11 da linha 2
	MOV 50h, #0CAh
	LCALL EnviaComando
	MOV 50h, 52h
	ORL 50h, #30h
	LCALL EscreveCaractere
RET

; Escreve DIREITA na primeira linha do LCD
ORG 04E0h
DireitaGanhou:
	LCALL LimpaTela
	MOV 50h, #80h
	LCALL EnviaComando

	MOV 50h, #44h
	LCALL EscreveCaractere
	MOV 50h, #49h
	LCALL EscreveCaractere
	MOV 50h, #52h
	LCALL EscreveCaractere
	MOV 50h, #45h
	LCALL EscreveCaractere
	MOV 50h, #49h
	LCALL EscreveCaractere
	MOV 50h, #54h
	LCALL EscreveCaractere
	MOV 50h, #41h
	LCALL EscreveCaractere
	LCALL Ganhou
	LCALL EscrevePlacar
RET

; Escreve ESQUERDA na primeira linha do LCD
ORG 0530h
EsquerdaGanhou:
	LCALL LimpaTela
	MOV 50h, #80h
	LCALL EnviaComando

	MOV 50h, #45h
	LCALL EscreveCaractere
	MOV 50h, #53h
	LCALL EscreveCaractere
	MOV 50h, #51h
	LCALL EscreveCaractere
	MOV 50h, #55h
	LCALL EscreveCaractere
	MOV 50h, #45h
	LCALL EscreveCaractere
	MOV 50h, #52h
	LCALL EscreveCaractere
	MOV 50h, #44h
	LCALL EscreveCaractere
	MOV 50h, #41h
	LCALL EscreveCaractere
	
	LCALL Ganhou
	LCALL EscrevePlacar
RET

; Escreve GANHOU na segunda linha do LCD
ORG 0580h
Ganhou:
; Move o cursor
	MOV 50h, #0C0h
	LCALL EnviaComando

	MOV 50h, #47h
	LCALL EscreveCaractere
	MOV 50h, #41h
	LCALL EscreveCaractere
	MOV 50h, #4Eh
	LCALL EscreveCaractere
	MOV 50h, #48h
	LCALL EscreveCaractere
	MOV 50h, #4Fh
	LCALL EscreveCaractere
	MOV 50h, #55h
	LCALL EscreveCaractere

	LCALL EscreveTrofeu

	LCALL Delay
	LCALL LimpaTela
RET

; Mostra o símbolo do troféu no LCD
ORG 05D0h
EscreveTrofeu:
; Primeira linha
	MOV 50h, #8Ch
	LCALL EnviaComando

	MOV 50h, #00h
	LCALL EscreveCaractere
	MOV 50h, #01h
	LCALL EscreveCaractere
	MOV 50h, #02h
	LCALL EscreveCaractere
	MOV 50h, #03h
	LCALL EscreveCaractere
; Segunda linha
	MOV 50h, #0CCh
	LCALL EnviaComando

	MOV 50h, #04h
	LCALL EscreveCaractere
	MOV 50h, #05h
	LCALL EscreveCaractere
	MOV 50h, #06h
	LCALL EscreveCaractere
	MOV 50h, #04h
	LCALL EscreveCaractere
RET

END