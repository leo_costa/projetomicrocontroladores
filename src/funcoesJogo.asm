; Arquivo com todas as funções responsáveis por fazer o jogo funcionar
; internamente/lógica do jogo
;=============================================================================
; Registradores importantes:
; 20h : Contém os bits de controle da direção da bola (cima/baixo direita/esquerda)
; R1  : Contém o endereço do registrador da matriz de jogo que a bola está
; 40h ~ 47h: Matriz de jogo, composta por 8 registradores de 8 bits
;=============================================================================
$INCLUDE(funcoesLCD.asm)

; Move a bolinha pela matriz essa função tambem calcula a colisão da bolinha 
; e incrementa o placar se alguém pontuar
ORG 0620h
MoveBola:
;Movimento vertical
	MOV A, @R1
	MOV @R1, #00h

	JB 20h.1, CIMA

;BAIXO
	RL A
	JNB ACC.7, FIM_VERTICAL
	JB ACC.7, INVERTE

CIMA:
	RR A
	JNB ACC.0, FIM_VERTICAL

INVERTE:
	CPL 20h.1

FIM_VERTICAL:
	MOV R3, ACC

;Movimento horizontal

JB 20h.0, DIREITA

ESQUERDA:
	MOV ACC, #41h
	CLR C
	SUBB A, R1

	JNZ DEC_R1E

	MOV ACC, R3
	ORL A, 40h

	JNB PSW.0, PONTO_E

	INC R1
	SJMP REBATE

    DEC_R1E:
		DEC R1
		SJMP FIM_HORIZONTAL

    PONTO_E:
		LCALL IncPontoD
		SJMP FIM_HORIZONTAL

DIREITA:
	MOV ACC, #46h
	CLR C
	SUBB A, R1

	JNZ INC_R1D

	MOV ACC, R3
	ORL A, 47h

	JNB PSW.0, PONTO_D

	DEC R1
	SJMP REBATE

	INC_R1D:
		INC R1
		SJMP FIM_HORIZONTAL

	PONTO_D:
		LCALL IncPontoE
		SJMP FIM_HORIZONTAL

REBATE:
	CPL 20h.0

FIM_HORIZONTAL:
	MOV A, R3
	MOV @R1, A
RET

; Reseta o jogo
ORG 0680h
ResetaJogo:
; Inicializa a matriz - Menos as raquetes
	MOV 41h, #00h
	MOV 42h, #00h
	MOV 43h, #00h
	MOV 44h, #01h
	MOV 45h, #00h
	MOV 46h, #00h
; Define em que registrador a bolinha esta
	MOV R1, #44h
; Zero o placar
	MOV 51h, #00h
	MOV 52h, #00h
	LCALL EscrevePontosEsquerda
	LCALL EscrevePontosDireita
RET

; Incrementa os pontos do jogador da esquerda, ao fazer um ponto
; o jogo é reiniciado com a bola próxima do jogador que pontuou e em direção
; ao adversário
ORG 06C0h
IncPontoE:
	MOV R3, #01h
	CLR 20h.1
	SETB 20h.0
	MOV R1, #41h
	INC 51h

	MOV A, #09h
	CLR C
	SUBB A, 51h

	JNC ESCREVE_ESQ
	LCALL EsquerdaGanhou
	LCALL ResetaJogo; Jogador da esquerda venceu
	SJMP FIM_INC_E
ESCREVE_ESQ:
	LCALL EscrevePontosEsquerda
FIM_INC_E:
RET

; Incrementa os pontos do jogador da direita, ao fazer um ponto
; o jogo é reiniciado com a bola próxima do jogador que pontuou e em direção
; ao adversário
ORG 06F0h
IncPontoD:
	MOV R3, #01h
	CLR 20h.1
	CLR 20h.0
	MOV R1, #46h
	INC 52h

	MOV A, #09h
	CLR C
	SUBB A, 52h

	JNC ESCREVE_DIR
	LCALL DireitaGanhou
	LCALL ResetaJogo; Jogador da direita venceu
	SJMP FIM_INC_D
ESCREVE_DIR:
	LCALL EscrevePontosDireita
FIM_INC_D:
RET

; Função de debounce para o botão P2.0 (Sobe-Direita)
ORG 0720h
DebounceP20:
	JB P2.0,FIM_P20
	
	LCALL Delay15

	JB P2.0,FIM_P20
	
	SETB 21h.0

FIM_P20:	
RET

; Função de debounce para o botão P2.1 (Desce-Direita)
ORG 0730h
DebounceP21:
	JB P2.1,FIM_P21
	
	LCALL Delay15

	JB P2.1,FIM_P21
	
	SETB 21h.1

FIM_P21:	
RET

; Função de debounce para o botão P2.2 (Sobe-Esquerda)
ORG 0740h
DebounceP22:
	JB P2.2,FIM_P22
	
	LCALL Delay15

	JB P2.2,FIM_P22
	
	SETB 21h.2

FIM_P22:
RET

; Função de debounce para o botão P2.3 (Desce-Esquerda)
ORG 0750h
DebounceP23:
	JB P2.3,FIM_P23
	
	LCALL Delay15

	JB P2.3,FIM_P23
	
	SETB 21h.3

FIM_P23:
RET

; Sobe a raquete da direita
ORG 0770h
SobeDireita:
	CLR 21h.0
	CLR 21h.4

	CLR C
	MOV A, 47h
	SUBB A, #07h

	JZ FIM_SOBE_D

	MOV A, 47h
	RR A
	MOV 47h, A
FIM_SOBE_D:
RET

; Desce a raquete da direita
ORG 0790h
DesceDireita:
	CLR 21h.1
	CLR 21h.5

	CLR C
	MOV A, 47h
	SUBB A, #0E0h

	JZ FIM_DESCE_D

	MOV A, 47h
	RL A
	MOV 47h, A
FIM_DESCE_D:
RET

; Sobe a raquete da esquerda
ORG 07B0h
SobeEsquerda:
	CLR 21h.2
	CLR 21h.6


	CLR C
	MOV A, 40h
	SUBB A, #07h

	JZ FIM_SOBE_E

	MOV A, 40h
	RR A
	MOV 40h, A
FIM_SOBE_E:
RET

; Desce a raquete da esquerda
ORG 07D0h
DesceEsquerda:
	CLR 21h.3
	CLR 21h.7

	CLR C
	MOV A, 40h
	SUBB A, #0E0h

	JZ FIM_DESCE_E

	MOV A, 40h
	RL A
	MOV 40h, A
FIM_DESCE_E:
RET	

; Analisa se o jogador soltou o botão P2.0
ORG 07F0h
AnalisaP20:
	JNB 21h.0, END_A_P20

	JNB P2.0, END_A_P20

	SETB 21h.4
END_A_P20:
RET

; Analisa se o jogador soltou o botão P2.1
ORG 0800h
AnalisaP21:
	JNB 21h.1, END_A_P21

	JNB P2.1, END_A_P21

	SETB 21h.5
END_A_P21:
RET

; Analisa se o jogador soltou o botão P2.2
ORG 0810h
AnalisaP22:
	JNB 21h.2, END_A_P22

	JNB P2.2, END_A_P22

	SETB 21h.6
END_A_P22:
RET

; Analisa se o jogador soltou o botão P2.3 
ORG 0820h
AnalisaP23:
	JNB 21h.3, END_A_P23

	JNB P2.3, END_A_P23

	SETB 21h.7
END_A_P23:
RET

; Lógica de acionamento do botão P2.0
ORG 0830h
RaqueteDireitaCima:
	
	JB P2.0, MEIO_D_C

	JB 21h.0, MEIO_D_C
	
	LCALL DebounceP20
MEIO_D_C:
	LCALL AnalisaP20

	JNB 21h.4, FIM_D_C

	LCALL SobeDireita
FIM_D_C:
RET

; Lógica de acionamento do botão P2.1
ORG 0850h
RaqueteDireitaBaixo:
	
	JB P2.1, MEIO_D_B

	JB 21h.1, MEIO_D_B
	
	LCALL DebounceP21

MEIO_D_B:
	LCALL AnalisaP21

	JNB 21h.5, FIM_D_B

	LCALL DesceDireita
FIM_D_B:
RET

; Lógica de acionamento do botão P2.2
ORG 0870h
RaqueteEsquerdaCima:
	
	JB P2.2, MEIO_E_C

	JB 21h.2, MEIO_E_C
	
	LCALL DebounceP22
MEIO_E_C:
	LCALL AnalisaP22

	JNB 21h.6, FIM_E_C

	LCALL SobeEsquerda
FIM_E_C:
RET

; Lógica de acionamento do botão P2.3
ORG 0890h
RaqueteEsquerdaBaixo:
	
	JB P2.3, MEIO_E_B

	JB 21h.3, MEIO_E_B
	
	LCALL DebounceP23
MEIO_E_B:
	LCALL AnalisaP23

	JNB 21h.7, FIM_E_B

	LCALL DesceEsquerda
FIM_E_B:
RET

END