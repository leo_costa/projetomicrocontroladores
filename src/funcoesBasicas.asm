; Arquivo com todas as outras funções que não são funções de jogo, delay ou
; do LCD
;=============================================================================
$INCLUDE(funcoesJogo.asm)

ORG 0000h
LJMP Principal


ORG 000Bh; Interrupção do timer 0
	SJMP TrataT0

; Tratamento da interrupção do Timer0
ORG 0080h
TrataT0:
	INC 60h
	MOV TH0, #15h
	MOV TL0, #0A0h
RETI

; Mostra o conteúdo das posições 40h a 47h na matriz de LEDs
ORG 1310h
MostraMatriz:
	MOV R0, #40h
	MOV P1, #0FEh
	MOV R4, #08h
ADDM:
	MOV P3, @R0
	LCALL Delay1 ;Delay do tempo de resposta dos LEDs da matriz
	MOV A, P1
	RL A
	MOV P1, A

	INC R0
	DJNZ R4, ADDM
MOV P3, 40h
RET

; Função principal
ORG 1330h
Principal:
	MOV SP, #70h
; Define os ports P1 e P3 como saída
	MOV P1, #00h
	MOV P3, #00h
; Define o nibble superior do port P2 como entrada
	MOV P2, #0Fh
; Inicializa as raquetes
	MOV 40h, #07h
	MOV 47h, #07h

; Configura o Timer 0 no modo 1 (16bits)
	MOV TMOD, #01h
	MOV TCON, #10h
; Inicializa o Timer 0 com estes valores para gerar uma base
; de tempo de 60ms
	MOV TH0, #15h
	MOV TL0, #0A0h
	MOV IE, #82h

; Reset do registrador de controle do botões de movimentação
	MOV 21h, #00h

	LCALL IniciaLCD
	LCALL CriaSimboloTrofeu
	LCALL EscrevePlacar
	LCALL ResetaJogo
LOOP:
; Repete 10 vezes a contagem do Timer0 para mover a bola, 
; isso gera um período de 0.6s
	MOV A, #09h
	CLR C
	SUBB A,  60h
	JNC CONT
	MOV 60h, #00h
	LCALL MoveBola

CONT:
    ; Detecta o acionamento dos botões de movimentação das raquetes
	LCALL RaqueteDireitaCima
	LCALL RaqueteDireitaBaixo
	LCALL RaqueteEsquerdaCima
	LCALL RaqueteEsquerdaBaixo

    ; Atualiza a matriz de LEDs
	LCALL MostraMatriz

	SJMP LOOP
END