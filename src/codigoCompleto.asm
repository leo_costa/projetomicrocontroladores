; Arquivo com todas as outras funções de delay por software
;=============================================================================

; Delay de 15ms +
ORG 0100h
Delay15:
	MOV R5, #0FFh
D151:
	MOV R6, #1Bh
D152:
	DJNZ R6, D152
	DJNZ R5, D151
RET

; Delay de 4.1ms +
ORG 0120h
Delay4:
	MOV R5, #0FFh
D41:
	MOV R6, #07h
D42:
	DJNZ R6, D42
	DJNZ R5, D41
RET

; Delay de 1ms +
ORG 0130h
Delay1:
	MOV R5, #02h
D11:
	MOV R6, #0FFh
D12:
	DJNZ R6, D12
	DJNZ R5, D11
RET

; Delay de 100us +
ORG 0140h
Delay01:
	MOV R5, #32h
D011:
	DJNZ R5, D011
RET

; Delay para o pulso de Enable do LCD
ORG 0147h
DelayEnable:
	MOV R5, #5Ah
DE:
	DJNZ R5, DE
RET

; Delay longo
ORG 0150h
Delay:
	MOV R5, #1Ch
DL1:
	MOV R6, #0FFh
DL2:
	MOV R7, #0FFh
DL3:
	DJNZ  R7, DL3
	DJNZ  R6, DL2
	DJNZ  R5, DL1
RET

; Arquivo com todas as funções responsáveis por lidar com o
; display de LCD
;=============================================================================
; Pinos utilizados
; P0 |7  6   5   4  3   2   1   0
; LCD|E  RW  RS  -  D7  D6  D5  D4
;=============================================================================
; $INCLUDE(delays.asm)

; Gera um pulso no pino de Enable do LCD
ORG 0170h
PulsarEnable:
	SETB P0.7
	LCALL DelayEnable
	CLR P0.7
RET

; Inicializa o LCD
ORG 0180h
IniciaLCD:
	LCALL Delay15

	MOV P0, #03h
	LCALL PulsarEnable

	LCALL Delay4

	MOV P0, #03h
	LCALL PulsarEnable

	LCALL Delay01

	MOV P0, #03h
	LCALL PulsarEnable
	
	LCALL Delay01

	MOV P0, #02h
	LCALL PulsarEnable

	LCALL Delay01

	MOV P0, #02h
	LCALL PulsarEnable

	LCALL Delay01

	MOV P0, #0Ch ; 0 0 0 0 N F X X
	LCALL PulsarEnable

	LCALL Delay01

	MOV P0, #00h
	LCALL PulsarEnable

	LCALL Delay01

	MOV P0, #08h
	LCALL PulsarEnable

	LCALL Delay01

	MOV P0, #00h
	LCALL PulsarEnable

	LCALL Delay01

	MOV P0, #01h
	LCALL PulsarEnable

	LCALL Delay01

	MOV P0, #00h
	LCALL PulsarEnable
	
	LCALL Delay01

	MOV P0, #06h ; 0 0 0 0 0 1 I/D S
	LCALL PulsarEnable

	LCALL Delay01

	MOV P0, #00h
	LCALL PulsarEnable

	LCALL Delay1

	MOV P0, #0Ch ;0 0 0 0 1 D C B
	LCALL PulsarEnable

	LCALL Delay1
RET

; Envia o comando contido no reg. 50h
ORG 0220h
EnviaComando:
;High Nibble
	MOV P0, #00h
	MOV A, 50h
	ANL A, #0F0h
	RR A
	RR A
	RR A
	RR A
	ORL P0, A
	LCALL PulsarEnable
	LCALL Delay1
;Low Nibble
	MOV P0, #00h
	MOV A, 50h
	ANL A, #0Fh
	ORL P0, A
	LCALL PulsarEnable
	LCALL Delay1
RET

; Escreve o caractere armazenado no reg. 50h
ORG 0270h
EscreveCaractere:
;High Nibble
	MOV P0, #20h
	MOV A, 50h
	ANL A, #0F0h
	RR A
	RR A
	RR A
	RR A
	ORL P0, A
	LCALL PulsarEnable
	LCALL Delay1
;Low Nibble
	MOV P0, #20h
	MOV A, 50h
	ANL A, #0Fh
	ORL P0, A
	LCALL PulsarEnable
	LCALL Delay1
RET

; Limpa a tela do LCD
ORG 02C0h
LimpaTela:
	MOV 50h, #01h
	LCALL EnviaComando
RET

; Cria o símbolo de troféu na CGRAM do LCD
ORG 02E0h
CriaSimboloTrofeu:
	MOV 50h, #40h
	LCALL EnviaComando

; A
	MOV 50h, #14h
	LCALL EscreveCaractere
	MOV 50h, #08h
	LCALL EscreveCaractere
	MOV 50h, #14h
	LCALL EscreveCaractere
	MOV 50h, #00h
	LCALL EscreveCaractere
	MOV 50h, #03h
	LCALL EscreveCaractere
	MOV 50h, #02h
	LCALL EscreveCaractere
	MOV 50h, #02h
	LCALL EscreveCaractere
	MOV 50h, #01h
	LCALL EscreveCaractere

	MOV 50h, #48h
	LCALL EnviaComando
; B
	MOV 50h, #03h
	LCALL EscreveCaractere
	MOV 50h, #04h
	LCALL EscreveCaractere
	MOV 50h, #0Fh
	LCALL EscreveCaractere
	MOV 50h, #18h
	LCALL EscreveCaractere
	MOV 50h, #08h
	LCALL EscreveCaractere
	MOV 50h, #08h
	LCALL EscreveCaractere
	MOV 50h, #08h
	LCALL EscreveCaractere
	MOV 50h, #08h
	LCALL EscreveCaractere

	MOV 50h, #50h
	LCALL EnviaComando
; C
	MOV 50h, #18h
	LCALL EscreveCaractere
	MOV 50h, #04h
	LCALL EscreveCaractere
	MOV 50h, #1Eh
	LCALL EscreveCaractere
	MOV 50h, #03h
	LCALL EscreveCaractere
	MOV 50h, #02h
	LCALL EscreveCaractere
	MOV 50h, #02h
	LCALL EscreveCaractere
	MOV 50h, #02h
	LCALL EscreveCaractere
	MOV 50h, #02h
	LCALL EscreveCaractere

	MOV 50h, #58h
	LCALL EnviaComando
; D
	MOV 50h, #05h
	LCALL EscreveCaractere
	MOV 50h, #02h
	LCALL EscreveCaractere
	MOV 50h, #05h
	LCALL EscreveCaractere
	MOV 50h, #00h
	LCALL EscreveCaractere
	MOV 50h, #18h
	LCALL EscreveCaractere
	MOV 50h, #08h
	LCALL EscreveCaractere
	MOV 50h, #08h
	LCALL EscreveCaractere
	MOV 50h, #10h
	LCALL EscreveCaractere

	MOV 50h, #60h
	LCALL EnviaComando
; E
	MOV 50h, #00h
	LCALL EscreveCaractere
	MOV 50h, #00h
	LCALL EscreveCaractere
	MOV 50h, #04h
	LCALL EscreveCaractere
	MOV 50h, #0Ch
	LCALL EscreveCaractere
	MOV 50h, #04h
	LCALL EscreveCaractere
	MOV 50h, #04h
	LCALL EscreveCaractere
	MOV 50h, #04h
	LCALL EscreveCaractere
	MOV 50h, #04h
	LCALL EscreveCaractere

	MOV 50h, #68h
	LCALL EnviaComando
; F
	MOV 50h, #18h
	LCALL EscreveCaractere
	MOV 50h, #0Ch
	LCALL EscreveCaractere
	MOV 50h, #06h
	LCALL EscreveCaractere
	MOV 50h, #01h
	LCALL EscreveCaractere
	MOV 50h, #01h
	LCALL EscreveCaractere
	MOV 50h, #01h
	LCALL EscreveCaractere
	MOV 50h, #03h
	LCALL EscreveCaractere
	MOV 50h, #07h
	LCALL EscreveCaractere

	MOV 50h, #70h
	LCALL EnviaComando
; G
	MOV 50h, #03h
	LCALL EscreveCaractere
	MOV 50h, #06h
	LCALL EscreveCaractere
	MOV 50h, #0Ch
	LCALL EscreveCaractere
	MOV 50h, #10h
	LCALL EscreveCaractere
	MOV 50h, #10h
	LCALL EscreveCaractere
	MOV 50h, #10h
	LCALL EscreveCaractere
	MOV 50h, #18h
	LCALL EscreveCaractere
	MOV 50h, #1Ch
	LCALL EscreveCaractere

	MOV 50h, #80h
	LCALL EnviaComando
RET

; Escreve PLACAR no LCD
ORG 0480h
EscrevePlacar:
; Move o cursor
	MOV 50h, #85h
	LCALL EnviaComando
; Escreve PLACAR
	MOV 50h, #50h; P
	LCALL EscreveCaractere
	MOV 50h, #4Ch; L
	LCALL EscreveCaractere
	MOV 50h, #41h; A
	LCALL EscreveCaractere
	MOV 50h, #43h; C
	LCALL EscreveCaractere
	MOV 50h, #41h; A
	LCALL EscreveCaractere
	MOV 50h, #52h; R
	LCALL EscreveCaractere
RET

; Escreve no LCD a pontuação do jogador da esquerda
; A pontuação fica armazenada no reg. 51h
ORG 04C0h
EscrevePontosEsquerda:
; Move o cursor
	MOV 50h, #0C5h
	LCALL EnviaComando
	MOV 50h, 51h
	ORL 50h, #30h
	LCALL EscreveCaractere
RET

; Escreve no LCD a pontuação do jogador da direita
; A pontuação fica armazenada no reg. 52h
ORG 04D0h
EscrevePontosDireita:
; Coloca o cursor na posicao 11 da linha 2
	MOV 50h, #0CAh
	LCALL EnviaComando
	MOV 50h, 52h
	ORL 50h, #30h
	LCALL EscreveCaractere
RET

; Escreve DIREITA na primeira linha do LCD
ORG 04E0h
DireitaGanhou:
	LCALL LimpaTela
	MOV 50h, #80h
	LCALL EnviaComando

	MOV 50h, #44h
	LCALL EscreveCaractere
	MOV 50h, #49h
	LCALL EscreveCaractere
	MOV 50h, #52h
	LCALL EscreveCaractere
	MOV 50h, #45h
	LCALL EscreveCaractere
	MOV 50h, #49h
	LCALL EscreveCaractere
	MOV 50h, #54h
	LCALL EscreveCaractere
	MOV 50h, #41h
	LCALL EscreveCaractere
	LCALL Ganhou
	LCALL EscrevePlacar
RET

; Escreve ESQUERDA na primeira linha do LCD
ORG 0530h
EsquerdaGanhou:
	LCALL LimpaTela
	MOV 50h, #80h
	LCALL EnviaComando

	MOV 50h, #45h
	LCALL EscreveCaractere
	MOV 50h, #53h
	LCALL EscreveCaractere
	MOV 50h, #51h
	LCALL EscreveCaractere
	MOV 50h, #55h
	LCALL EscreveCaractere
	MOV 50h, #45h
	LCALL EscreveCaractere
	MOV 50h, #52h
	LCALL EscreveCaractere
	MOV 50h, #44h
	LCALL EscreveCaractere
	MOV 50h, #41h
	LCALL EscreveCaractere
	
	LCALL Ganhou
	LCALL EscrevePlacar
RET

; Escreve GANHOU na segunda linha do LCD
ORG 0580h
Ganhou:
; Move o cursor
	MOV 50h, #0C0h
	LCALL EnviaComando

	MOV 50h, #47h
	LCALL EscreveCaractere
	MOV 50h, #41h
	LCALL EscreveCaractere
	MOV 50h, #4Eh
	LCALL EscreveCaractere
	MOV 50h, #48h
	LCALL EscreveCaractere
	MOV 50h, #4Fh
	LCALL EscreveCaractere
	MOV 50h, #55h
	LCALL EscreveCaractere

	LCALL EscreveTrofeu

	LCALL Delay
	LCALL LimpaTela
RET

; Mostra o símbolo do troféu no LCD
ORG 05D0h
EscreveTrofeu:
; Primeira linha
	MOV 50h, #8Ch
	LCALL EnviaComando

	MOV 50h, #00h
	LCALL EscreveCaractere
	MOV 50h, #01h
	LCALL EscreveCaractere
	MOV 50h, #02h
	LCALL EscreveCaractere
	MOV 50h, #03h
	LCALL EscreveCaractere
; Segunda linha
	MOV 50h, #0CCh
	LCALL EnviaComando

	MOV 50h, #04h
	LCALL EscreveCaractere
	MOV 50h, #05h
	LCALL EscreveCaractere
	MOV 50h, #06h
	LCALL EscreveCaractere
	MOV 50h, #04h
	LCALL EscreveCaractere
RET

; Arquivo com todas as funções responsáveis por fazer o jogo funcionar
; internamente/lógica do jogo
;=============================================================================
; Registradores importantes:
; 20h : Contém os bits de controle da direção da bola (cima/baixo direita/esquerda)
; R1  : Contém o endereço do registrador da matriz de jogo que a bola está
; 40h ~ 47h: Matriz de jogo, composta por 8 registradores de 8 bits
;=============================================================================
; $INCLUDE(funcoesLCD.asm)

; Move a bolinha pela matriz essa função tambem calcula a colisão da bolinha 
; e incrementa o placar se alguém pontuar
ORG 0620h
MoveBola:
;Movimento vertical
	MOV A, @R1
	MOV @R1, #00h

	JB 20h.1, CIMA

;BAIXO
	RL A
	JNB ACC.7, FIM_VERTICAL
	JB ACC.7, INVERTE

CIMA:
	RR A
	JNB ACC.0, FIM_VERTICAL

INVERTE:
	CPL 20h.1

FIM_VERTICAL:
	MOV R3, ACC

;Movimento horizontal

JB 20h.0, DIREITA

ESQUERDA:
	MOV ACC, #41h
	CLR C
	SUBB A, R1

	JNZ DEC_R1E

	MOV ACC, R3
	ORL A, 40h

	JNB PSW.0, PONTO_E

	INC R1
	SJMP REBATE

    DEC_R1E:
		DEC R1
		SJMP FIM_HORIZONTAL

    PONTO_E:
		LCALL IncPontoD
		SJMP FIM_HORIZONTAL

DIREITA:
	MOV ACC, #46h
	CLR C
	SUBB A, R1

	JNZ INC_R1D

	MOV ACC, R3
	ORL A, 47h

	JNB PSW.0, PONTO_D

	DEC R1
	SJMP REBATE

	INC_R1D:
		INC R1
		SJMP FIM_HORIZONTAL

	PONTO_D:
		LCALL IncPontoE
		SJMP FIM_HORIZONTAL

REBATE:
	CPL 20h.0

FIM_HORIZONTAL:
	MOV A, R3
	MOV @R1, A
RET

; Reseta o jogo
ORG 0680h
ResetaJogo:
; Inicializa a matriz - Menos as raquetes
	MOV 41h, #00h
	MOV 42h, #00h
	MOV 43h, #00h
	MOV 44h, #01h
	MOV 45h, #00h
	MOV 46h, #00h
; Define em que registrador a bolinha esta
	MOV R1, #44h
; Zero o placar
	MOV 51h, #00h
	MOV 52h, #00h
	LCALL EscrevePontosEsquerda
	LCALL EscrevePontosDireita
RET

; Incrementa os pontos do jogador da esquerda, ao fazer um ponto
; o jogo é reiniciado com a bola próxima do jogador que pontuou e em direção
; ao adversário
ORG 06C0h
IncPontoE:
	MOV R3, #01h
	CLR 20h.1
	SETB 20h.0
	MOV R1, #41h
	INC 51h

	MOV A, #09h
	CLR C
	SUBB A, 51h

	JNC ESCREVE_ESQ
	LCALL EsquerdaGanhou
	LCALL ResetaJogo; Jogador da esquerda venceu
	SJMP FIM_INC_E
ESCREVE_ESQ:
	LCALL EscrevePontosEsquerda
FIM_INC_E:
RET

; Incrementa os pontos do jogador da direita, ao fazer um ponto
; o jogo é reiniciado com a bola próxima do jogador que pontuou e em direção
; ao adversário
ORG 06F0h
IncPontoD:
	MOV R3, #01h
	CLR 20h.1
	CLR 20h.0
	MOV R1, #46h
	INC 52h

	MOV A, #09h
	CLR C
	SUBB A, 52h

	JNC ESCREVE_DIR
	LCALL DireitaGanhou
	LCALL ResetaJogo; Jogador da direita venceu
	SJMP FIM_INC_D
ESCREVE_DIR:
	LCALL EscrevePontosDireita
FIM_INC_D:
RET

; Função de debounce para o botão P2.0 (Sobe-Direita)
ORG 0720h
DebounceP20:
	JB P2.0,FIM_P20
	
	LCALL Delay15

	JB P2.0,FIM_P20
	
	SETB 21h.0

FIM_P20:	
RET

; Função de debounce para o botão P2.1 (Desce-Direita)
ORG 0730h
DebounceP21:
	JB P2.1,FIM_P21
	
	LCALL Delay15

	JB P2.1,FIM_P21
	
	SETB 21h.1

FIM_P21:	
RET

; Função de debounce para o botão P2.2 (Sobe-Esquerda)
ORG 0740h
DebounceP22:
	JB P2.2,FIM_P22
	
	LCALL Delay15

	JB P2.2,FIM_P22
	
	SETB 21h.2

FIM_P22:
RET

; Função de debounce para o botão P2.3 (Desce-Esquerda)
ORG 0750h
DebounceP23:
	JB P2.3,FIM_P23
	
	LCALL Delay15

	JB P2.3,FIM_P23
	
	SETB 21h.3

FIM_P23:
RET

; Sobe a raquete da direita
ORG 0770h
SobeDireita:
	CLR 21h.0
	CLR 21h.4

	CLR C
	MOV A, 47h
	SUBB A, #07h

	JZ FIM_SOBE_D

	MOV A, 47h
	RR A
	MOV 47h, A
FIM_SOBE_D:
RET

; Desce a raquete da direita
ORG 0790h
DesceDireita:
	CLR 21h.1
	CLR 21h.5

	CLR C
	MOV A, 47h
	SUBB A, #0E0h

	JZ FIM_DESCE_D

	MOV A, 47h
	RL A
	MOV 47h, A
FIM_DESCE_D:
RET

; Sobe a raquete da esquerda
ORG 07B0h
SobeEsquerda:
	CLR 21h.2
	CLR 21h.6


	CLR C
	MOV A, 40h
	SUBB A, #07h

	JZ FIM_SOBE_E

	MOV A, 40h
	RR A
	MOV 40h, A
FIM_SOBE_E:
RET

; Desce a raquete da esquerda
ORG 07D0h
DesceEsquerda:
	CLR 21h.3
	CLR 21h.7

	CLR C
	MOV A, 40h
	SUBB A, #0E0h

	JZ FIM_DESCE_E

	MOV A, 40h
	RL A
	MOV 40h, A
FIM_DESCE_E:
RET	

; Analisa se o jogador soltou o botão P2.0
ORG 07F0h
AnalisaP20:
	JNB 21h.0, END_A_P20

	JNB P2.0, END_A_P20

	SETB 21h.4
END_A_P20:
RET

; Analisa se o jogador soltou o botão P2.1
ORG 0800h
AnalisaP21:
	JNB 21h.1, END_A_P21

	JNB P2.1, END_A_P21

	SETB 21h.5
END_A_P21:
RET

; Analisa se o jogador soltou o botão P2.2
ORG 0810h
AnalisaP22:
	JNB 21h.2, END_A_P22

	JNB P2.2, END_A_P22

	SETB 21h.6
END_A_P22:
RET

; Analisa se o jogador soltou o botão P2.3 
ORG 0820h
AnalisaP23:
	JNB 21h.3, END_A_P23

	JNB P2.3, END_A_P23

	SETB 21h.7
END_A_P23:
RET

; Lógica de acionamento do botão P2.0
ORG 0830h
RaqueteDireitaCima:
	
	JB P2.0, MEIO_D_C

	JB 21h.0, MEIO_D_C
	
	LCALL DebounceP20
MEIO_D_C:
	LCALL AnalisaP20

	JNB 21h.4, FIM_D_C

	LCALL SobeDireita
FIM_D_C:
RET

; Lógica de acionamento do botão P2.1
ORG 0850h
RaqueteDireitaBaixo:
	
	JB P2.1, MEIO_D_B

	JB 21h.1, MEIO_D_B
	
	LCALL DebounceP21

MEIO_D_B:
	LCALL AnalisaP21

	JNB 21h.5, FIM_D_B

	LCALL DesceDireita
FIM_D_B:
RET

; Lógica de acionamento do botão P2.2
ORG 0870h
RaqueteEsquerdaCima:
	
	JB P2.2, MEIO_E_C

	JB 21h.2, MEIO_E_C
	
	LCALL DebounceP22
MEIO_E_C:
	LCALL AnalisaP22

	JNB 21h.6, FIM_E_C

	LCALL SobeEsquerda
FIM_E_C:
RET

; Lógica de acionamento do botão P2.3
ORG 0890h
RaqueteEsquerdaBaixo:
	
	JB P2.3, MEIO_E_B

	JB 21h.3, MEIO_E_B
	
	LCALL DebounceP23
MEIO_E_B:
	LCALL AnalisaP23

	JNB 21h.7, FIM_E_B

	LCALL DesceEsquerda
FIM_E_B:
RET

; Arquivo com todas as outras funções que não são funções de jogo, delay ou
; do LCD
;=============================================================================
; $INCLUDE(funcoesJogo.asm)

ORG  0000h
LJMP Principal


ORG 000Bh; Interrupção do timer 0
	SJMP TrataT0

; Tratamento da interrupção do Timer0
ORG 0080h
TrataT0:
	INC 60h
	MOV TH0, #15h
	MOV TL0, #0A0h
RETI

; Mostra o conteúdo das posições 40h a 47h na matriz de LEDs
ORG 1310h
MostraMatriz:
	MOV R0, #40h
	MOV P1, #0FEh
	MOV R4, #08h
ADDM:
	MOV P3, @R0
	LCALL Delay1 ;Delay do tempo de resposta dos LEDs da matriz
	MOV A, P1
	RL A
	MOV P1, A

	INC R0
	DJNZ R4, ADDM
MOV P3, 40h
RET

; Função principal
ORG 1330h
Principal:
	MOV SP, #70h
; Define os ports P1 e P3 como saída
	MOV P1, #00h
	MOV P3, #00h
; Define o nibble superior do port P2 como entrada
	MOV P2, #0Fh
; Inicializa as raquetes
	MOV 40h, #07h
	MOV 47h, #07h

; Configura o Timer 0 no modo 1 (16bits)
	MOV TMOD, #01h
	MOV TCON, #10h
; Inicializa o Timer 0 com estes valores para gerar uma base
; de tempo de 60ms
	MOV TH0, #15h
	MOV TL0, #0A0h
	MOV IE, #82h

; Reset do registrador de controle do botões de movimentação
	MOV 21h, #00h

	LCALL IniciaLCD
	LCALL CriaSimboloTrofeu
	LCALL EscrevePlacar
	LCALL ResetaJogo
LOOP:
; Repete 10 vezes a contagem do Timer0 para mover a bola, 
; isso gera um período de 0.6s
	MOV A, #09h
	CLR C
	SUBB A,  60h
	JNC CONT
	MOV 60h, #00h
	LCALL MoveBola

CONT:
    ; Detecta o acionamento dos botões de movimentação das raquetes
	LCALL RaqueteDireitaCima
	LCALL RaqueteDireitaBaixo
	LCALL RaqueteEsquerdaCima
	LCALL RaqueteEsquerdaBaixo

    ; Atualiza a matriz de LEDs
	LCALL MostraMatriz

	SJMP LOOP
END