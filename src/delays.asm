; Arquivo com todas as outras funções de delay por software
;=============================================================================

; Delay de 15ms +
ORG 0100h
Delay15:
	MOV R5, #0FFh
D151:
	MOV R6, #1Bh
D152:
	DJNZ R6, D152
	DJNZ R5, D151
RET

; Delay de 4.1ms +
ORG 0120h
Delay4:
	MOV R5, #0FFh
D41:
	MOV R6, #07h
D42:
	DJNZ R6, D42
	DJNZ R5, D41
RET

; Delay de 1ms +
ORG 0130h
Delay1:
	MOV R5, #02h
D11:
	MOV R6, #0FFh
D12:
	DJNZ R6, D12
	DJNZ R5, D11
RET

; Delay de 100us +
ORG 0140h
Delay01:
	MOV R5, #32h
D011:
	DJNZ R5, D011
RET

; Delay para o pulso de Enable do LCD
ORG 0147h
DelayEnable:
	MOV R5, #5Ah
DE:
	DJNZ R5, DE
RET

; Delay longo
ORG 0150h
Delay:
	MOV R5, #1Ch
DL1:
	MOV R6, #0FFh
DL2:
	MOV R7, #0FFh
DL3:
	DJNZ  R7, DL3
	DJNZ  R6, DL2
	DJNZ  R5, DL1
RET

END